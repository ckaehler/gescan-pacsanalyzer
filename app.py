import paho.mqtt.client as mqtt
import ipaddress
import os
import subprocess
import threading
import shlex
import sys
from datetime import datetime
from functools import partial

def popen_and_call(on_exit, *popen_args, **popen_kwargs):
    def run_in_thread(on_exit, popen_args, popen_kwargs):
        proc = subprocess.Popen(*popen_args, **popen_kwargs)
        proc.wait()
        on_exit()
        return
    thread = threading.Thread(target=run_in_thread, args=(on_exit, popen_args, popen_kwargs))
    thread.start()
    return thread

def my_callback(filename):
    coreFile = "/data/"+filename
    etmFile = coreFile+"_00"
    if os.path.exists(etmFile):
        subprocess.run(["cat " + etmFile + " >> " + coreFile], shell=True)
        os.remove(etmFile)
    print("PACSAnalyzer Complete: "+str(filename))

def on_connect(client, userdata, flags, rc):
    print("Connected to mqtt server with result code "+str(rc))
    client.subscribe("gescan/pacsanalyzer")

def on_message(client, userdata, message):
    try:
        payload = message.payload.decode()
        ip = ipaddress.ip_address(payload)
        print("recieved message: " ,str(payload))
        date = datetime.now().strftime("%Y%m%dT%I%M%S")
        filename = f"GPA-{date}.txt"
        command = shlex.split('wine /root/PacsAnalyzer.exe /ip=' + payload + ' /out=/data/' + filename + ' /get')
        popen_and_call(partial(my_callback, filename), command, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except ValueError:
        print("invalid IP recieved: " ,str(payload))
    except:
        print("Exception raised")

if not os.path.isdir('/root/.wine32'):
    print("Initializing .wine32 directory ...")
    subprocess.run(["winecfg"], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    print("Finished initializing.")

mqttBroker = os.environ.get("GPA_MQTT_BROKER", "broker.hivemq.com")
print("Using MQTT Broker: "+str(mqttBroker))

client = mqtt.Client("Gescan Client")
client.on_connect=on_connect
client.on_message=on_message

client.connect(mqttBroker)
client.loop_forever()
