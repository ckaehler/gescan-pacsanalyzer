# gescan/pacsanalyzer

This repository contains all the files required to get PACSAnalyzer running on an Emerson edge stack.  Please be aware that at present Emerson does not allow SRTP through the embedded network card on the CPL410 (192.168.180.2) so this architecture is not possible.

## Installation

To install this do the following:

```Shell
cd ~
mkdir -p pacedge/gescan-pacsanalyzer/data
docker build -t gescan/pacsanalyzer https://bitbucket.org/ckaehler/gescan-pacsanalyzer.git
```

This will build and create the gescan/pacsanalyzer image and prepare it for you.  There is a sample docker-compose.yml entry in the repository under docker-compose.yml.sample which you can use to add to the default pacedge/docker-compose.yml on the Emerson edge stack.  As well as a sample node-red flow for testing.

Special thanks to Tom Behnke (@tombeh) of Cimtec who helped with the initial concept and did a lot of the heavy lifting.  This wouldnt have been possible without his contributions.
