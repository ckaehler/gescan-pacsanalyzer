ARG BASE_IMAGE="ubuntu"
ARG TAG="20.04"
FROM ${BASE_IMAGE}:${TAG}

# Create our DATA directory
RUN mkdir /data

# Install prerequisites
RUN apt-get update \
    && DEBIAN_FRONTEND="noninteractive" apt-get install -y --no-install-recommends \
        apt-transport-https \
        ca-certificates \
        cabextract \
        git \
        gnupg \
        gosu \
        gpg-agent \
        locales \
        p7zip \
        python3 \
        python3-pip \
        python3-paho-mqtt \
        tzdata \
        unzip \
        wget \
        winbind \
        xvfb \
        zenity \
    && rm -rf /var/lib/apt/lists/*

# Install wine
ARG WINE_BRANCH="stable"
RUN wget -nv -O- https://dl.winehq.org/wine-builds/winehq.key | APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 apt-key add - \
    && echo "deb https://dl.winehq.org/wine-builds/ubuntu/ $(grep VERSION_CODENAME= /etc/os-release | cut -d= -f2) main" >> /etc/apt/sources.list \
    && dpkg --add-architecture i386 \
    && apt-get update \
    && DEBIAN_FRONTEND="noninteractive" apt-get install -y --install-recommends winehq-${WINE_BRANCH} \
    && rm -rf /var/lib/apt/lists/*

# Disable fixme warnings
ENV WINEDEBUG=fixme-all

# Setup wine prefix
ENV WINEARCH=win32 
ENV WINEPREFIX=/root/.wine32 
#RUN winecfg #winecfg operates differently if done in dockerfile vs in container, fails here.

# Install winetricks
#RUN wget -nv -O /usr/bin/winetricks https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks \
#    && chmod +x /usr/bin/winetricks

WORKDIR /root
COPY PacsAnalyzer.exe ./
COPY app.py ./

CMD ["python3", "-u", "./app.py"]
